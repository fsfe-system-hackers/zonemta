# zoneMTA

FSFE's zoneMTA installation to send out mass emails with a low spam score (SPF, DKIM).

Closely tied to our [Mailtrain installation](https://git.fsfe.org/fsfe-system-hackers/mailtrain).
