FROM node:8-alpine

RUN apk update && apk upgrade && apk add --no-cache bash curl python make
RUN npm install --production zone-mta

WORKDIR /app

COPY config /app/config
COPY index.js /app


CMD ["node", "index.js", "--config=config/zonemta.toml"]

